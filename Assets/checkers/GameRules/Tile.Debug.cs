using System;
using UnityEngine;

namespace checkers
{
    public partial class Tile
    {
        #region FOR DEBUGGING PURPOUSES (Check TilesAttributeDrawer)
        Action<GameController>[] _states;
        Action<GameController>[] states
        {
            get
            {
                if (_states == null)
                {
                    _states = new Action<GameController>[]
                    {
                        MakeEmpty,
                        MakeRedPiece, MakeBlackPiece,
                        MakeRedQueenPiece, MakeBlackQueenPiece,
                    };
                }

                return _states;
            }
        }

        public void ChangeState(GameController game, TState state, Vector2Int coords)
        {
            while (state < 0)
            {
                state += states.Length;
            }

            states[(int)state % states.Length](game);
        }

        void MakeEmpty(GameController game)
        {
            game.ReturnPiece(this);
            MakeEmpty();
        }

        void MakeRedPiece(GameController game)
        {
            MakeEmpty(game);

            playerPiece = game.redPlayerPiecesBag.GetPiece(game);
            playerPiece.Idle();
            _playerColor = TPlayerColor.red;
            _isQueen = false;
        }

        void MakeBlackPiece(GameController game)
        {
            MakeEmpty(game);

            playerPiece = game.blackPlayerPiecesBag.GetPiece(game);
            playerPiece.Idle();
            _playerColor = TPlayerColor.black;
            _isQueen = false;
        }

        void MakeRedQueenPiece(GameController game)
        {
            MakeEmpty(game);

            playerPiece = game.redPlayerQueenPiecesBag.GetPiece(game);
            playerPiece.Idle();
            _playerColor = TPlayerColor.red;
            _isQueen = true;
        }

        void MakeBlackQueenPiece(GameController game)
        {
            MakeEmpty(game);

            playerPiece = game.blackPlayerQueenPiecesBag.GetPiece(game);
            playerPiece.Idle();
            _playerColor = TPlayerColor.black;
            _isQueen = true;
        }
        #endregion
    }
}