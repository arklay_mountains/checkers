using System.Collections.Generic;
using UnityEngine.Analytics;

namespace checkers
{
    public class Winner : GameState
    {
        public Winner(GameController game, TPlayerColor playerColor) : base(game, playerColor)
        {
            WriteText(game.uiTexts.winner);
            WriteTextInOpponent(game.uiTexts.loser);
        }
    }
}