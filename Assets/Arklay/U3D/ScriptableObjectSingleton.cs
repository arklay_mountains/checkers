﻿using UnityEngine;

public abstract class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObjectSingleton<T>
{
    static T __instance;
    public static T instance
    {
        get
        {
            if (__instance == null)
            {
                __instance = Resources.Load<T>(typeof(T).FullName);
                if(Application.isPlaying && __instance == null)
                {
                    __instance = CreateInstance<T>();
                }
            }
            return __instance;
        }
        private set
        {
            if (__instance == value)
                return;
            if (__instance != null)
                throw new System.InvalidOperationException(string.Format("Two singletons ({0}) in the scene!", __instance.GetType().FullName));
            __instance = value;
        }
    }
    public virtual void Awake()
    {
        instance = (T)this;
    }

    public virtual void OnDestroy()
    {
        __instance = null;
    }
}