using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace checkers
{
    public partial class GameController
    {
        const string playerPrefKey = "SavedState";
        void SaveState(string currentBoard, TPlayerColor nextPlayerColor)
        {
            // handle persistance
            var data = new Dictionary<string, object>();
            data["currentBoard"] = currentBoard;
            data["nextPlayer"] = nextPlayerColor;
            data[nameof(pastBoardPositions)] = pastBoardPositions;
            data[nameof(drawableTurns)] = drawableTurns;

            var dataStr = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            PlayerPrefs.SetString(playerPrefKey, dataStr);
            PlayerPrefs.Save();
        }

        TPlayerColor RestoreSavedGame()
        {
            var nextPlayer = TPlayerColor.red;
            var dataStr = PlayerPrefs.GetString(playerPrefKey);
            if (!string.IsNullOrEmpty(dataStr))
            {
                var data = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(dataStr);
                drawableTurns = (int)(long)data[nameof(drawableTurns)];
                pastBoardPositions.Clear();
                var pastBoardPositionsAux = data[nameof(pastBoardPositions)];

                nextPlayer = (TPlayerColor)(int)(long)data["nextPlayer"];
                var currentBoard = (string) data["currentBoard"];
                RestoreBoardPositions(currentBoard);
            }
            return nextPlayer;
        }

        void ClearSavedState()
        {
            PlayerPrefs.SetString(playerPrefKey, null);
            PlayerPrefs.Save();            
        }

        bool IsThereSavedGame()
        {
            return !string.IsNullOrEmpty(PlayerPrefs.GetString(playerPrefKey));
        }
    }
}