using U3D.Events;
using UnityEngine;

namespace U3D.Colliders
{
    [RequireComponent(typeof(Collider))]
    public class TriggerEvents : MonoBehaviour
    {
        Collider _linkedCollider;
        public Collider linkedCollider
        {
            get
            {
                if (_linkedCollider == null)
                    _linkedCollider = GetComponent<Collider>();
                return _linkedCollider;
            }
        }

        public UnityEventCollider onTriggerEnter = new UnityEventCollider();
        void OnTriggerEnter(Collider other)
        {
            onTriggerEnter?.Invoke(other);
        }

        public UnityEventCollider onTriggerStay = new UnityEventCollider();
        void OnTriggerStay(Collider other)
        {
            onTriggerStay?.Invoke(other);
        }
        
        public UnityEventCollider onTriggerExit = new UnityEventCollider();
        void OnTriggerExit(Collider other)
        {
            onTriggerExit?.Invoke(other);
        }

    }
}