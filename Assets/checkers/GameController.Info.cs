using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace checkers
{
    public partial class GameController
    {
        public Vector2Int GetCoordsFor(GameComponent gameComponent)
        {
            for (int yCoord = 0; yCoord < 8; yCoord++)
            {
                for (int xCoord = 0; xCoord < 8; xCoord++)
                {
                    if (tiles[xCoord, yCoord].boardTile == gameComponent || 
                        tiles[xCoord, yCoord].playerPiece == gameComponent)
                    {
                        return new Vector2Int(xCoord, yCoord);
                    }
                }                
            }
            throw new InvalidOperationException($"Cannot find player piece");
        }

        Tile GetTileFor(Vector2Int coords)
        {
            return tiles[coords.x, coords.y];
        }
        
        public BoardTile GetBoardTileFor(Vector2Int coords)
        {
            return GetTileFor(coords).boardTile;
        }
        public PlayerPiece GetPlayerPieceFor(Vector2Int coords)
        {
            return GetTileFor(coords).playerPiece;
        }
        
         List<Vector2Int> GetPlayerTiles(TPlayerColor playerColor)
        {
            var ret = new List<Vector2Int>();

            for (int xCoord = 0; xCoord < 8; xCoord++)
            {
                for (int yCoord = 0; yCoord < 8; yCoord++)
                {
                    var tile = tiles[xCoord, yCoord];
                    if (tile.playerPiece != null && tile.playerColor == playerColor)
                    {
                        ret.Add(new Vector2Int(xCoord, yCoord));
                    }
                }
            }
			
            return ret;
        }

        static TPlayerColor OppositeColor(TPlayerColor color)
        {
            return color == TPlayerColor.red
                ? TPlayerColor.black
                : TPlayerColor.red;
        }

        public List<Vector2Int> GetMoveablePieces(TPlayerColor playerColor)
        {
            var allPieces = GetPlayerTiles(playerColor);
            
            var normalMoves = new List<Vector2Int>();
            var killingMoves = new List<Vector2Int>();
            foreach (var coords in allPieces)
            {
                bool isKillingMove;
                var possibleMoves = PossibleMoves(coords, out isKillingMove);
                if (possibleMoves.Count > 0)
                {
                    if (isKillingMove)
                        killingMoves.Add(coords);
                    else
                        normalMoves.Add(coords);
                }
            }

            return killingMoves.Count > 0 ? killingMoves : normalMoves;
        }
        
        public Vector3 GetPositionForCoords(Vector2Int coords)
        {
            return new Vector3(
                (coords.x - 4) * boardTileWidth + boardTileWidth / 2f,
                0,
                (coords.y - 4) * boardTileWidth + boardTileWidth / 2f);
        }

    }
}