using Boo.Lang;
using Sirenix.Utilities;

namespace checkers
{
    public class ChoosePiece : GameState
    {
        readonly List<PlayerPiece> selectablePieces;
        public ChoosePiece(GameController game, TPlayerColor playerColor, PlayerPiece forceChoose = null) : base(game, playerColor)
        {
            WriteText(game.uiTexts.choosePiece);
            
            selectablePieces = new List<PlayerPiece>();

            if (forceChoose == null)
            {
                var availablePieces = game.GetMoveablePieces(playerColor);
                foreach (var coord in availablePieces)
                {
                    selectablePieces.Add(game.GetPlayerPieceFor(coord));
                }
            }
            else
            {
                selectablePieces.Add(forceChoose);
            }

            selectablePieces.ForEach(p => p.selectable.onSelected = PieceSelected);
        }

        void PieceSelected(GameComponent sender)
        {
            selectablePieces.ForEach(p =>
            {
                if (p != sender)
                {
                    p.Idle();
                }
            });
            game.currentState = new ChooseTile(this, (PlayerPiece)sender);
        }
    }
}