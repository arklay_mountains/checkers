# Israel Illan - Techincal Test

Technical test for Toptal

## Requirements

Create an AR Party game.

- Create an augmented reality app which places a party game on a flat surface and lets you play the game.

- You may choose a party game to your liking, but whichever you choose, you should replicate the rules of the game as 
closely as possible.

- Your highest priority should be optimizing the game to run smoothly.

- We don’t need professional graphics. However, if you can come up with something creative in this regard it’d be a plus.

- You must prepare your environment for the call so that you can easily present your app via screen share

## First approach

First approach was to recreate the "jenga" game, but getting the right "physical" feeling to push, pull and grab 
the pieces would need more too much time

## Game chosen

Decided to go for a less risky approach by choosing "checkers".

Following the rules outlined in http://www.wcdf.net/rules/rules_of_checkers_english.pdf

### Code structure

The code is structured in a way that could be reused as a framework allowing both:

- Re-skin the current game, changing the models, animations, particle systems
- Implement a different party game

#### GameController

Holds the basic fields that will be shown in the Unity editor and will allow to configure the game and give some 
feedback inside Unity:

- Gameobject pools for both types of tiles, normal player pieces and queen pieces.
- Current game state
- UI Text used to give feedback to the user

It will also expose the methods to start, end and refresh the game.

#### GameController.Rules & GameRules/Tile

Contains the abstract rules of the game, takes care of calculating the possible moves, and handles the turns.

Since checkers is a relatively simple game there is no need to overcomplicate it by delegating in other classes.

#### GameControler.Actions

Takes care of connecting the rules and the classes which will actually show the game to the player

#### GameControler.Info

Exposes methods used by the components which will represent the internal game to the user

#### GameController.Persistance

Handles saving/restoring the state of the game

#### BoardAttribute, Editor/BoardAttributeDrawer & GameRules/Tile.Debug

Helper class to show the inner game state in the unity editor allowing the developer to view and edit the player pieces
on the board, for debugging purposes.

#### UITexts

Scriptable object holding the texts that will be shown to the user at runtime. Keeping them in a scriptable object will
allow to use one of the many plugins existing in the unity asset store to localize the game in multiple languages

#### Editor/BuildDialog

Helper menu to build, push and launch the game in a android device via adb

#### GameState/...

Possible states of the game and the feedback given to the player

#### GameComponents/xxxBag

These game component pools are used to cache and reuse the visual elements that will be used in the game, such as:
- board tiles (2 types)
- standard player pieces (2 colors)
- queen player pieces (2 colors)

#### GameComponents/GameComponent

Base class inherited by all the visual components of the game.

Instead of relying heavily in inheritance (which is never a good idea in Unity), each visual component will rely in
Composition pattern and will require different components depending on its needed features: 

##### GameComponents/GameComponentSelectable

Will be required by visual components that the user can select

##### GameComponents/GameComponentMoveable

Will be required by visual components that will be moved across the board

##### GameComponents/GameComponentAnimation

Will be required by visual components that will give some feedback based on a animator controller

##### GameComponents/GameComponentAnimatedSelectable

Will be required by visual components that the user can select and the feedback will be given by an animation 
controller (this component will require the game object to include both: 
GameComponentSelectable and GameComponentAnimation)

##### GameComponents/BoardTile

Basic board tile, which can be selected by the user, therefore requires GameComponentSelectable behaviour to be 
attached to its game object

##### GameComponents/PlayerPiece

Base class for player pieces, it requires to include the GameComponentSelectable behaviour but not the moveable 
behaviour since it might be implemented in different ways, not just moving the piece directly on the board 
(eg: by using a navigation mesh)

#### GameComponents/BasicTiles/...

Contains the prefabs, materials and basic animations used by the basic tiles

#### GameComponents/BasicPlayerPieces/...

Contains the basic implementation of the player pieces, which consists just in the classic discs that move accross 
the board and give feedback using basic transform animations

#### GameComponents/CharacterPlayerPieces/...

Includes a more interesting aprroach for the player pieces that are reprsented by the same discs by default, but 
gives feedback by showing some characters that fight each other over the boards. It also includes some particle effects
and a system to ensure that each piece character is different to each other.
