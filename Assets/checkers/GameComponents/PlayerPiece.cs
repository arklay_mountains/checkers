using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using U3D.Animators;
using UnityEngine;

namespace checkers
{
    [RequireComponent(typeof(GameComponentSelectable))]
    public abstract class PlayerPiece : GameComponent
    {
        GameComponentSelectable _linkedGameComponentSelectable;
        public GameComponentSelectable selectable => _linkedGameComponentSelectable = _linkedGameComponentSelectable ? _linkedGameComponentSelectable : GetComponent<GameComponentSelectable>();
        public abstract Task MoveTo(Vector2Int to);
        public abstract Task Kill(Vector2Int killCoords, Vector2Int to);
        public abstract Task Die(Vector2Int killerCoords);
        public abstract Task BecomeQueen();

        public virtual void Revive()
        {
        }
    }
}