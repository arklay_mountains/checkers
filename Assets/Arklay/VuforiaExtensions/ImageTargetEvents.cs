﻿using System.Collections;
using System.Collections.Generic;
using U3D.Events;
using UnityEngine;
using Vuforia;

namespace U3D.VuforiaExtensions
{
	[RequireComponent(typeof(TrackableBehaviour)), DisallowMultipleComponent]
	public class ImageTargetEvents : MonoBehaviour, ITrackableEventHandler
	{
		TrackableBehaviour _trackable;
		TrackableBehaviour trackable
		{
			get
			{
				if (_trackable == null)
					_trackable = GetComponent<TrackableBehaviour>();
				return _trackable;
			}
		}

		void Start()
		{
			trackable.RegisterTrackableEventHandler(this);
			StartCoroutine(DelayedStart());
		}

		IEnumerator DelayedStart()
		{
			yield return new WaitForEndOfFrame();
			OnTrackableStateChanged(trackable.CurrentStatus, trackable.CurrentStatus);
		}

		void OnDestroy()
		{
			trackable.UnregisterTrackableEventHandler(this);
		}


		public UnityEventBool onTracking;
		public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
		{
			if (newStatus == TrackableBehaviour.Status.DETECTED ||
			    newStatus == TrackableBehaviour.Status.TRACKED ||
			    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
			{
				onTracking?.Invoke(true);
			}
			else 
				//(previousStatus == TrackableBehaviour.Status.TRACKED && newStatus == TrackableBehaviour.Status.NO_POSE)
				//(previousStatus == TrackableBehaviour.Status.UNKNOWN && newStatus == TrackableBehaviour.Status.NOT_FOUND)
			{
				onTracking?.Invoke(false);
			}
		}
	}
}