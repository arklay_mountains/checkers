﻿using System.Collections;
using System.Collections.Generic;
using U3D.Cameras;
using UnityEngine;
using Vuforia;

namespace U3D.VuforiaExtensions
{
	[RequireComponent(typeof(VuforiaBehaviour))]
	[DisallowMultipleComponent]
	public class DebugCamera : MonoBehaviour
	{
#if UNITY_EDITOR
		VuforiaBehaviour _linkedVuforiaBehaviour;

		VuforiaBehaviour linkedVuforiaBehaviour
		{
			get
			{
				if (_linkedVuforiaBehaviour == null)
					_linkedVuforiaBehaviour = GetComponent<VuforiaBehaviour>();
				return _linkedVuforiaBehaviour;
			}
		}

		FlyCamera flyCamera;
		void Start()
		{
			if (!targetFound)
			{
				transform.position = new Vector3(0f, 0.64f, -0.53f);
				transform.eulerAngles = new Vector3(43f, 0f, 0f);
			}
			flyCamera = gameObject.AddComponent<FlyCamera>();
			TargetFound(targetFound);
		}

		bool targetFound = false;
		public void TargetFound(bool v)
		{
			targetFound = v;
			if(flyCamera!= null)
				flyCamera.enabled = !v;
		}
#endif
	}
}