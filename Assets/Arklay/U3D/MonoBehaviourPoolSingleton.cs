﻿using UnityEngine;

public class MonoBehaviourPoolSingleton<T, R> : MonoBehaviourPool<R> where T : MonoBehaviourPoolSingleton<T, R> where R : Component
{
    static T __instance = null;
    public static T instance
    {
        get
        {
            if (__instance != null) return __instance;

            __instance = FindObjectOfType<T>();
            if (__instance == null)
            {
                __instance = new GameObject(typeof(T).Name).AddComponent<T>();
            }
            Debug.Assert(__instance != null, "Cannot create singleton for " + typeof(T).Name);
            return __instance;
        }
        private set
        {
            if (__instance == value)
                return;
            if (__instance != null)
                throw new System.InvalidOperationException("Two singletons in the scene!");
            __instance = value;
        }
    }
    void Awake()
    {
        instance = (T)this;
    }
}
