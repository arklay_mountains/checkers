using UnityEngine;

namespace U3D.Cameras
{
    public class FlyCamera : MonoBehaviour
    {
        /*
         * Based on Windex's flycam script found here: http://forum.unity3d.com/threads/fly-cam-simple-cam-script.67042/
         * C# conversion created by Ellandar
         * Improved camera made by LookForward
         * Modifications created by Angryboy
         * 1) Have to hold right-click to rotate
         * 2) Made variables public for testing/designer purposes
         * 3) Y-axis now locked (as if space was always being held)
         * 4) Q/E keys are used to raise/lower the camera
         *
         * Another Modification created by micah_3d
         * 1) adding an isColliding bool to allow camera to collide with world objects, terrain etc.
         */

        public float mainSpeed = 2.5f; //regular speed
        public float shiftAdd = 10; //multiplied by how long shift is held.  Basically running
        public float maxShift = 1000.0f; //Maximum speed when holdin gshift

        public float camSens = 0.25f; //How sensitive it with mouse

        //private Vector3 lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)
        private float totalRun = 1.0f;

        private bool
            isRotating = false; // Angryboy: Can be called by other things (e.g. UI) to see if camera is rotating

        private float speedMultiplier; // Angryboy: Used by Y axis to match the velocity on X/Z axis

        public float mouseSensitivity = 5.0f; // Mouse rotation sensitivity.
        private float rotationY = 0.0f;

        private bool isMoving = false;

        private Vector2 lastPos = Vector2.zero;

        void Update()
        {

            // Angryboy: Hold right-mouse button to rotate
            if (Input.GetMouseButtonDown(1))
            {
                isRotating = true;
            }

            if (Input.GetMouseButtonUp(1))
            {
                isRotating = false;
            }

            if (isRotating)
            {
                // Made by LookForward
                // Angryboy: Replaced min/max Y with numbers, not sure why we had variables in the first place
                float rotationX = transform.localEulerAngles.y +
                                  (Input.mousePosition.x - lastPos.x) * mouseSensitivity * .1f;
                rotationY += (Input.mousePosition.y - lastPos.y) * mouseSensitivity * .1f;
                rotationY = Mathf.Clamp(rotationY, -90, 90);
                transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0.0f);
            }

            //Keyboard commands
            Vector3 p = GetBaseInput();
            if (Input.GetKey(KeyCode.LeftShift))
            {
                p = p * totalRun * shiftAdd;
                p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
                p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
                p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
                // Angryboy: Use these to ensure that Y-plane is affected by the shift key as well
                speedMultiplier = totalRun * shiftAdd * Time.deltaTime;
                speedMultiplier = Mathf.Clamp(speedMultiplier, -maxShift, maxShift);
            }
            else
            {
                p = p * totalRun * mainSpeed;
                speedMultiplier = totalRun * mainSpeed * Time.deltaTime; // Angryboy: More "correct" speed
                speedMultiplier = Mathf.Clamp(speedMultiplier, -maxShift, maxShift);
            }

            if (this.isMoving)
            {
                totalRun += Time.deltaTime;
            }
            else
                totalRun = Mathf.Clamp(totalRun * 0.5f, 1f, 1000f);

            p = p * Time.deltaTime;

            // Angryboy: Removed key-press requirement, now perma-locked to the Y plane
            Vector3 newPosition = transform.position; //If player wants to move on X and Z axis only
            transform.Translate(p);
            newPosition.x = transform.position.x;
            newPosition.z = transform.position.z;

            // Angryboy: Manipulate Y plane by using Q/E keys
            if (Input.GetKey(KeyCode.Q))
            {
                transform.position = transform.position + new Vector3(0, -speedMultiplier, 0);
            }

            if (Input.GetKey(KeyCode.E))
            {
                transform.position = transform.position + new Vector3(0, speedMultiplier, 0);
            }

            lastPos = Input.mousePosition;
        }

        // Angryboy: Can be called by other code to see if camera is rotating
        // Might be useful in UI to stop accidental clicks while turning?
        public bool amIRotating()
        {
            return isRotating;
        }

        private Vector3 GetBaseInput()
        {
            //returns the basic values, if it's 0 than it's not active.
            Vector3 p_Velocity = new Vector3();
            this.isMoving = false;
            if (Input.GetKey(KeyCode.W))
            {
                p_Velocity += new Vector3(0, 0, 1);
                this.isMoving = true;
            }

            if (Input.GetKey(KeyCode.S))
            {
                p_Velocity += new Vector3(0, 0, -1);
                this.isMoving = true;
            }

            if (Input.GetKey(KeyCode.A))
            {
                p_Velocity += new Vector3(-1, 0, 0);
                this.isMoving = true;
            }

            if (Input.GetKey(KeyCode.D))
            {
                p_Velocity += new Vector3(1, 0, 0);
                this.isMoving = true;
            }

            return p_Velocity;
        }
    }
}