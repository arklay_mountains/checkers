﻿using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class MonoBehaviourPool<T> : MonoBehaviour where T : Component
{
    Transform _activeContainer;
    Transform _inactiveContainer;

    Transform activeContainer
    {
        get
        {
            if (_activeContainer == null)
            {
                _activeContainer = new GameObject("Active").transform;
                _activeContainer.SetParent(this.transform, false);
            }

            return _activeContainer;
        }
    }
    Transform inactiveContainer
    {
        get
        {
            if (_inactiveContainer == null)
            {
                _inactiveContainer = new GameObject("Inactive").transform;
                _inactiveContainer.SetParent(this.transform, false);
                _inactiveContainer.gameObject.SetActive(false);

                while (_inactiveContainer.childCount < nbOfInitialObjects)
                {
                    CreateObject();            
                }
            }

            return _inactiveContainer;
        }
    }

    [Required]
    public T objectPrefab;

    public int nbOfInitialObjects = 100;

    readonly Queue<T> inactiveObjects = new Queue<T>();
    protected readonly List<T> activeObjects = new List<T>();

    protected virtual T CreateObject()
    {
        T obj = Instantiate<T>(objectPrefab);
        obj.name = $"{objectPrefab.name} {inactiveContainer.childCount + 1}";
        obj.transform.SetParent(inactiveContainer, false);
        inactiveObjects.Enqueue(obj);
        obj.gameObject.SetLayerRecursive(LayerMask.LayerToName(this.gameObject.layer));
        return obj;
    }

    protected virtual T ActivateObject()
    {
        if (inactiveObjects.Count == 0)
        {
            CreateObject();
        }
        T ret = inactiveObjects.Dequeue();
        activeObjects.Add(ret);
        ret.transform.SetParent(activeContainer, false);
        ret.gameObject.SetActive(true);
        return ret;
    }

    protected virtual void DeactivateObject(T obj)
    {
        activeObjects.Remove(obj);
        inactiveObjects.Enqueue(obj);
        obj.transform.SetParent(inactiveContainer, false);
        obj.gameObject.SetActive(false);
    }

    protected void DeactivateAllObjects()
    {
        T[] aux = activeObjects.ToArray();
        foreach (T i in aux)
            DeactivateObject(i);
    }
}
