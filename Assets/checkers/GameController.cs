﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace checkers
{
	[DisallowMultipleComponent]
	public partial class GameController : MonoBehaviour
	{
		public bool autoStartGame;
		[Required]
		public UITexts uiTexts;

		void OnEnable()
		{
			if (playing) // to avoid warning
				playing = false;
			StartCoroutine(StartDelayed());
		}
		IEnumerator StartDelayed()
		{
			yield return new WaitForEndOfFrame();
			if (IsThereSavedGame())
			{
				try
				{
					StartGame(RestoreSavedGame);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
//					ClearSavedState();
				}
			}
#if UNITY_EDITOR
			else
			{
				if (autoStartGame)
					StartGame();
			}
#endif

		}

		[Required]
		[HideInPlayMode]
		public Canvas mainMenu;
		[Required]
		[HideInPlayMode]
		public Canvas inGameMenu;

		bool playing;

		public void StartGame()
		{
			StartGame(null);
		}
		void StartGame(Func<TPlayerColor> boardSetup)
		{
			mainMenu.gameObject.SetActive(false);
			inGameMenu.gameObject.SetActive(true);

			playing = true;

			pastBoardPositions.Clear();
			drawableTurns = 0;
			
			SetupTiles();
			TPlayerColor nextPlayer = TPlayerColor.red;
			if (boardSetup != null)
			{
				nextPlayer = boardSetup.Invoke();
			}
			NextTurn(nextPlayer);
		}
		public void EndGame()
		{
			mainMenu.gameObject.SetActive(true);
			inGameMenu.gameObject.SetActive(false);

			playing = false;

			CleanupTiles();
			ClearSavedState();
		}

		public void ResetComponents()
		{
			foreach (var tile in tiles)
			{
				tile.boardTile.Idle();
				tile.playerPiece?.Idle();
			}
		}
		

		float? _boardTileWidth;
		public float boardTileWidth
		{
			get
			{
				if (_boardTileWidth == null)
				{
					var go = Instantiate(redTilesBag.objectPrefab.gameObject); 
					var collider = go.GetComponent<Collider>();
					_boardTileWidth = collider.bounds.size.x;
					
					DestroyImmediate(go);
				}
				return _boardTileWidth.Value;
			}
		}
		
		[Required]
		[HideInPlayMode]
		public BoardTilesBag redTilesBag, blackTilesBag;

		[Required]
		[HideInPlayMode]
		public PlayerPiecesBag redPlayerPiecesBag, blackPlayerPiecesBag;
		[Required]
		[HideInPlayMode]
		public PlayerPiecesBag redPlayerQueenPiecesBag, blackPlayerQueenPiecesBag;

		[ReadOnly] 
		public GameState currentState;

		[Required]
		[HideInPlayMode]
		public Text redPlayerText, blackPlayerText;
	}
}
