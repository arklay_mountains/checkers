using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace U3D.Threading
{
    /// <summary>
    /// Dispatches actions into the main thread.
    /// </summary>
    public class Dispatcher : MonoBehaviourSingleton<Dispatcher>
    {
        [RuntimeInitializeOnLoadMethod]
        static void RuntimeInitializeOnLoadMethod()
        {
            var _ = instance;
            mainThread = System.Threading.Thread.CurrentThread;
        }
        
        static System.Threading.Thread mainThread;
        public bool inMainThread
        {
            // if the getter throws NullReferenceException you're probably trying to use Dispatcher from Editor scripts
            // in this case pass EditorDispatcher.instance as a parameter in RunInMainThread and ContinueInMainThreadWith methods
            get
            {
                if (mainThread == null)
                    return false;
                return mainThread.Equals(System.Threading.Thread.CurrentThread);
            }
        }

        public override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(this);
        }

        public readonly Queue<Action> queue = new Queue<Action>();
        void Update()
        {
            Queue<Action> aux;
            lock (queue)
            {
                aux = new Queue<Action>(queue);
                queue.Clear();
            }
            while(aux.Count > 0)
            {
                aux.Dequeue()?.Invoke();
            }
        }

        /// <summary>
        /// Execute the Action in the main thread as soon as posible.
        /// </summary>
        /// <param name="a">Action to be executed.</param>
        public void ToMainThread(Action a)
        {
            if (queue.Count > 10)
            {
                UnityEngine.Debug.LogWarning($"[Dispatcher] Too many enqueued actions: {queue.Count}", this);
            }

            if (inMainThread)
            {
                a();
            }
            else
            {
                lock (queue)
                {
                    queue.Enqueue(a);
                }
            }
        }

        /// <summary>
        /// Executes the Action in the main thread as soon as posible
        /// and returns a Task which monitors its execution.
        /// </summary>
        /// <returns>Task monintoring the execution of the action.</returns>
        /// <param name="a">Action to be executed.</param>
        public Task TaskToMainThread(Action a)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            ToMainThread(() =>
            {
                a();
                tcs.SetResult(true);
            });
            return tcs.Task;
        }
        public Task<TResult> TaskToMainThread<TResult>(Func<TResult> f)
        {
            TaskCompletionSource<TResult> tcs = new TaskCompletionSource<TResult>();
            ToMainThread(() =>
            {
                tcs.SetResult(f());
            });
            return tcs.Task;
        }

        /// <summary>
        /// Execute the Action in the main thread after a delay.
        /// </summary>
        /// <param name="seconds">Execution delay, in seconds.</param>
        /// <param name="a">Action to be executed.</param>
        public void ToMainThreadAfterDelay(System.Double seconds, Action a)
        {
            instance.ToMainThread(() =>
            {
                instance.LaunchCoroutine(instance.ExecuteDelayed(seconds, a));
            });
        }
        System.Collections.IEnumerator ExecuteDelayed(System.Double seconds, Action a)
        {
            yield return new WaitForSeconds((float)seconds);
            ToMainThread(a);
        }

        public static void Initialize()
        {
            Dispatcher d = Dispatcher.instance;
            d = null;
        }

        /// <summary>
        /// Executes the coroutine passed as parameter in the main thread.
        /// </summary>
        /// <param name="firstIterationResult">Coroutine to be executed.</param>
        public void LaunchCoroutine(IEnumerator firstIterationResult)
        {
            instance.StartCoroutine(firstIterationResult);
        }
    }

}