using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.Analytics;

namespace checkers
{
    public class ChooseTile : GameState
    {
        PlayerPiece playerPiece;
        readonly List<BoardTile> selectableTiles;

        public ChooseTile(GameState previous, PlayerPiece playerPiece) : base(previous)
        {
            WriteText(game.uiTexts.chooseTile);

            this.playerPiece = playerPiece;
            var playerPieceCoords = game.GetCoordsFor(playerPiece);

            var possibleMoves = game.PossibleMoves(playerPieceCoords);
            selectableTiles = new List<BoardTile>();
            foreach (var possibleMoveCoord in possibleMoves)
            {
                var boardTile = game.GetBoardTileFor(possibleMoveCoord);
                boardTile.selectable.onSelected = (sender) => BoardTileSelected(sender);
                selectableTiles.Add(boardTile);
            }
        }

        async void BoardTileSelected(GameComponent sender)
        {
            selectableTiles.ForEach(p =>
            {
                p.Idle();
            });
            await game.ProcessMove(playerPiece.coords, sender.coords);
        }
    }
}