using U3D.Events;
using UnityEngine;

namespace U3D.Colliders
{
    [RequireComponent(typeof(Collider))]
    public class CollisionEvents : MonoBehaviour
    {
        Collider _linkedCollider;
        public Collider linkedCollider
        {
            get
            {
                if (_linkedCollider == null)
                    _linkedCollider = GetComponent<Collider>();
                return _linkedCollider;
            }
        }

        public UnityEventCollision onCollisionEnter = new UnityEventCollision();
        void OnCollisionEnter(Collision other)
        {
            onCollisionEnter?.Invoke(other);
        }

        public UnityEventCollision onCollisionStay = new UnityEventCollision();
        void OnCollisionStay(Collision other)
        {
            onCollisionStay?.Invoke(other);
        }
        
        public UnityEventCollision onCollisionExit = new UnityEventCollision();
        void OnCollisionExit(Collision other)
        {
            onCollisionExit?.Invoke(other);
        }

    }
}