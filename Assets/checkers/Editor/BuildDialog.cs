﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

static class BuildDialog
{
    [MenuItem("Build/Build, upload and launch")]
    static void BuildAndUpload()
    {
        EditorTools.ClearConsole();
        Debug.Log("-------------------- Start Build --------------------------------");
        
        if (KillApp() &&
            Build() && 
            PushToAndroid())
        {
            LaunchApp();
        }
        Debug.Log("-----------------------------------------------------------------");
    }

    static string adbLocation
    {
        get
        {
            var adbLocation = Path.Combine(EditorPrefs.GetString("AndroidSdkRoot"), "platform-tools/adb.exe");
            if (string.IsNullOrEmpty(adbLocation) || !File.Exists(adbLocation))
            {
                var path = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData))
                    .FullName;
                if (Environment.OSVersion.Version.Major >= 6)
                {
                    path = Directory.GetParent(path).ToString();
                }

                path = Path.Combine(path, "AppData/Local/Android/sdk/platform-tools/adb.exe");
                if (File.Exists(path))
                {
                    EditorPrefs.SetString("AndroidSdkRoot", path);
                    adbLocation = path;
                }
                else
                {
                    adbLocation = null;
                }
            }

            return adbLocation;
        }
    }
    
    static bool ExecuteADB(string arguments)
    {
        if (string.IsNullOrEmpty(adbLocation))
        {
            Debug.LogError("Cannot find adb location");
            return false;
        }


        var adb = new System.Diagnostics.Process();
        adb.StartInfo = new System.Diagnostics.ProcessStartInfo
        {
            FileName = adbLocation,
            Arguments = arguments,
            WorkingDirectory = Path.GetDirectoryName(adbLocation),
            UseShellExecute = false,
            RedirectStandardError = true,
            RedirectStandardOutput = true,
        };
        if (!adb.Start())
        {
            EditorUtility.DisplayDialog("ERROR", "Adb push failed", "d'oh!");
        }

        adb.WaitForExit();

        var output = adb.StandardOutput.ReadToEnd();
        if (!string.IsNullOrEmpty(output))
        {
            Debug.Log($"{arguments}:\n{output}");
        }
        var errors = adb.StandardError.ReadToEnd();
        if (!string.IsNullOrEmpty(errors) && errors != output)
        {
            Debug.LogError($"{arguments}:\n{errors}");
            return false;
        }

        return true;
    }

    [MenuItem("Build/Kill app")]
    static bool KillApp()
    {
        return ExecuteADB($"shell am force-stop {PlayerSettings.applicationIdentifier}");
    }
    static string buildPath => Path.Combine(Application.dataPath, "../Android/Builds");
    static string binaryPath => Path.Combine(buildPath, "checkers.apk");
    static string binaryFullPath => Path.GetFullPath(binaryPath);
    [MenuItem("Build/Build")]
    static bool Build()
    {
        if (EditorUserBuildSettings.activeBuildTarget != BuildTarget.Android)
        {
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
        }

		if (!Directory.Exists (buildPath)) 
		{
			Directory.CreateDirectory (buildPath);
		}

        var options = BuildOptions.Development;

        var scenes = EditorBuildSettings.scenes.Where(s => s.enabled).Select(s => s.path).ToArray();
        var result = BuildPipeline.BuildPlayer(scenes, 
			binaryFullPath,
            BuildTarget.Android, 
			options);

        if (result.summary.result != UnityEditor.Build.Reporting.BuildResult.Succeeded)
        {
            Debug.LogError(result);
            return false;
        }

        return true;
    }
    [MenuItem("Build/Push to Android")]
    static bool PushToAndroid()
    {
        return ExecuteADB($"install -r \"{binaryFullPath}\"");
    }
    [MenuItem("Build/Launch")]
    static bool LaunchApp()
    {
        return ExecuteADB(
            $"shell monkey -p {PlayerSettings.applicationIdentifier} -c android.intent.category.LAUNCHER 1");
    }
}
