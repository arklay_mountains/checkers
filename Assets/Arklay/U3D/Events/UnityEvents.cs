﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace U3D.Events
{
    [Serializable]
    public class UnityEventBool : UnityEvent<bool> { }
    [Serializable]
    public class UnityEventFloat : UnityEvent<float> { }
    [Serializable]
    public class UnityEventInt : UnityEvent<int> { }
    [Serializable]
    public class UnityEventString : UnityEvent<string> { }
    [Serializable]
    public class UnityEventVector2 : UnityEvent<Vector2> { }
    [Serializable]
    public class UnityEventVector3 : UnityEvent<Vector3> { }
    [Serializable]
    public class UnityEventObject : UnityEvent<object> { }
    [Serializable]
    public class UnityEventException : UnityEvent<Exception> { }
    [Serializable]
    public class UnityEventColor : UnityEvent<Color> { }
    [Serializable]
    public class UnityEventTimeSpan : UnityEvent<TimeSpan> { }
    [Serializable]
    public class UnityEventTexture2D : UnityEvent<Texture2D> { }
    [Serializable]
    public class UnityEventRenderTexture : UnityEvent<RenderTexture> { }
    [Serializable]
    public class UnityEventCollider : UnityEvent<Collider> { }
    [Serializable]
    public class UnityEventCollision : UnityEvent<Collision> { }
}