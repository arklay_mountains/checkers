using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace checkers
{
    [RequireComponent(typeof(Collider))]
    [DisallowMultipleComponent]
    public class GameComponentSelectable : MonoBehaviour
    {
        GameComponent _linkedGameComponent;
        GameComponent linkedGameComponent => _linkedGameComponent = _linkedGameComponent ? _linkedGameComponent : GetComponent<GameComponent>();
        void Start()
        {
            linkedGameComponent.onIdle += Idle;
            onSelected = _onSelected;
        }

        Collider _linkedCollider;
        Collider linkedCollider => _linkedCollider = _linkedCollider ? _linkedCollider : GetComponent<Collider>();

        public event Action<bool> onSelectableChanged;
        public event Action<bool> onSelectedChanged;
        
        Action<GameComponent> _onSelected;
        public Action<GameComponent> onSelected
        {
            get { return _onSelected; }
            set
            {
                _onSelected = value;
                linkedCollider.enabled = onSelected != null;
                onSelectableChanged?.Invoke(selectable);
            }
        }
        public bool selectable => onSelected != null;

        bool _selected;

        public bool selected
        {
            get { return _selected; }
            set
            {
                _selected = value; 
                onSelectedChanged?.Invoke(selected);
            }
        }

        void Idle()
        {
            onSelected = null;
            selected = false;
        }

        void OnMouseDown()
        {
            selected = true;
            linkedCollider.enabled = false;
            onSelected?.Invoke(this.linkedGameComponent);
            onSelected = null;
        }
    }
}