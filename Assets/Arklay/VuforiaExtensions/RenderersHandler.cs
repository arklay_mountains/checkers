using UnityEngine;

namespace U3D.VuforiaExtensions
{
    public class RenderersHandler : MonoBehaviour
    {
        public void OnTracking(bool tracked)
        {
            var renderers = GetComponentsInChildren<Renderer>(true);
            foreach (var renderer in renderers)
            {
                renderer.enabled = tracked;
            }

            var canvases = GetComponentsInChildren<Canvas>(true);
            foreach (var canvas in canvases)
            {
                canvas.enabled = tracked;
            }
        }
    }
}