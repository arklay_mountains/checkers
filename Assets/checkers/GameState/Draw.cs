using System.Collections.Generic;
using UnityEngine.Analytics;

namespace checkers
{
    public class Draw : GameState
    {
        public Draw(GameController game, string reason) : base(game, TPlayerColor.red)
        {
            WriteText(game.uiTexts.draw.Replace("%%REASON%%", reason));
            WriteTextInOpponent(game.uiTexts.draw.Replace("%%REASON%%", reason));
        }
    }
}