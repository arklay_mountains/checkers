using UnityEngine;

public static class GameObjectExtensions
{
    public static void SetLayerRecursive(this GameObject me, string name)
    {
        me.SetLayerRecursive(LayerMask.NameToLayer(name));
    }
    public static void SetLayerRecursive(this GameObject me, int layer)
    {
        me.layer = layer;
        foreach (Transform i in me.transform)
        {
            i.gameObject.SetLayerRecursive(layer);
        }
    }
    public static T EnsureComponent<T>(this GameObject me) where T: Component
    {
        T ret = me.GetComponent<T>();
        if (ret == null)
        {
            ret = me.AddComponent<T>();
        }

        return ret;
    }
}