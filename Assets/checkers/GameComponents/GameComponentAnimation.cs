using System;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using U3D.Animators;
using UnityEngine;

namespace checkers
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    public class GameComponentAnimation : MonoBehaviour
    {        
        Animator _linkedAnimator;
        public Animator animator => _linkedAnimator = _linkedAnimator ? _linkedAnimator : GetComponent<Animator>();
        
        AnimatorStateEvents[] animatorEvents;
        void OnEnable()
        {
            animatorEvents = animator.GetBehaviours<AnimatorStateEvents>();
            foreach (var animatorEvent in animatorEvents)
            {
                animatorEvent.onStateExit += AnimatorStateExited;
            }
        }
        void OnDisable()
        {
            foreach (var animatorEvent in animatorEvents)
            {
                animatorEvent.onStateExit += AnimatorStateExited;
            }
            animator.Rebind(); 
        }
        
        Action onAnimatorStateReached;
        void AnimatorStateExited(Animator arg1, AnimatorStateInfo arg2, int arg3)
        {
            onAnimatorStateReached?.Invoke();
            onAnimatorStateReached = null;
        }

        public Task PlayAnimation(Enum name)
        {
            var tcs = new TaskCompletionSource<object>();
            onAnimatorStateReached = () => tcs.SetResult(null);
            animator.SetTrigger(name.ToString());
            return tcs.Task;
        }
        public void RaiseTrigger(Enum name)
        {
            animator.SetTrigger(name.ToString());
        }
    }
}