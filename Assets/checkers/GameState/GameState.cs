using UnityEngine.UI;

namespace checkers
{
    public abstract class GameState
    {
        protected readonly TPlayerColor playerColor;
        protected readonly TPlayerColor oppositePlayerColor;
        protected readonly GameController game;
        readonly Text textBox;
        readonly Text opponentTextBox;
        
        protected GameState(GameController game, TPlayerColor playerColor)
        {
            game.redPlayerText.gameObject.SetActive(false);
            game.blackPlayerText.gameObject.SetActive(false);
            if (playerColor == TPlayerColor.red)
            {
                textBox = game.redPlayerText;
                oppositePlayerColor = TPlayerColor.black;
                opponentTextBox = game.blackPlayerText;
            }
            else
            {
                textBox = game.blackPlayerText;
                oppositePlayerColor = TPlayerColor.red;
                opponentTextBox = game.redPlayerText;
            }
            this.game = game;
            this.playerColor = playerColor;
        }
        protected GameState(GameState previous) : this(previous.game, previous.playerColor)
        {
        }

        protected void WriteText(string t)
        {
            textBox.text = t.Replace("%%PLAYER_COLOR%%", playerColor.ToString());
            textBox.gameObject.SetActive(true);
        }
        protected void WriteTextInOpponent(string t)
        {
            opponentTextBox.text = t.Replace("%%PLAYER_COLOR%%", oppositePlayerColor.ToString());
            opponentTextBox.gameObject.SetActive(true);
        }
    }
}