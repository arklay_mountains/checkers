using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace checkers
{
    public class UITexts : ScriptableObject
    {
#if UNITY_EDITOR
        [MenuItem("Assets/Create/Checkers texts")]
        public static void CreateNewUITexts()
        {
            var profile = ScriptableObject.CreateInstance<UITexts>();

            string path = AssetDatabase.GetAssetPath (Selection.activeObject);
            if (path == "") 
            {
                path = "Assets/checkers";
            } 
            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New UI Texts.asset");
            AssetDatabase.CreateAsset (profile, assetPathAndName);
            AssetDatabase.SaveAssets ();
            AssetDatabase.Refresh();
        }
#endif
        
        [InfoBox("Global macros:\n%%PLAYER_COLOR%% : player's color [red/black]", InfoMessageType.Info)]
        [TextArea]
        public string choosePiece = "%%PLAYER_COLOR%% player, it's your turn, choose a piece to move";
        [TextArea] 
        public string chooseTile = "great choice! where do you want to move it to?";
        [TextArea] 
        public string winner = "%%PLAYER_COLOR%% player you won! congratulations!";
        [TextArea] 
        public string loser = "%%PLAYER_COLOR%% player you lost, better luck next time";

        [InfoBox("Draw macros:\n%%REASON%% : player's color [red/black]", InfoMessageType.Info)]
        [TextArea] 
        public string draw= "This game is a draw, play another one! (%%REASON%%)";
        [TextArea] 
        public string drawReason3TimesSamePositions= "Same board positions reached 3 times or more";
        [TextArea] 
        public string drawReason40turnsWithoutForwardMoveByNotQueenNorKills= "There hasn't been any forward move (except by queens) nor any kill for 40 turns or more";
    }
}