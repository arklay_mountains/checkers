using System;
using System.Threading.Tasks;
using DG.Tweening;
using Sirenix.OdinInspector;
using U3D.Animators;
using UnityEngine;

namespace checkers
{
    [DisallowMultipleComponent]
    public class GameComponentMoveable : MonoBehaviour
    {
        public Task LookTo(Vector3 nextPosition, float duration)
        {
            var deltaMove = nextPosition - transform.position;
            var prevRotation = transform.localEulerAngles;
            var nextRotation = Vector3.SignedAngle(transform.forward, deltaMove, Vector3.up) - prevRotation.y;

            var tcs = new TaskCompletionSource<object>();
            transform.DORotate(Vector3.up * nextRotation, duration)
                .OnComplete(() => { tcs.SetResult(null); });
            return tcs.Task;
        }

        Task Rotate(Vector3 eulerAngle, float duration)
        {
            var tcs = new TaskCompletionSource<object>();
            transform.DOLocalRotate(eulerAngle, duration)
                .OnComplete(() => { tcs.SetResult(null); });
            return tcs.Task;
        }

        Task MoveToWithoutRotating(Vector3 nextPosition, float duration)
        {
            var tcs = new TaskCompletionSource<object>();
            transform.DOLocalMove(nextPosition, duration).SetEase(Ease.Linear)
                .OnComplete(() => { tcs.SetResult(null); });
            return tcs.Task;
        }

        public async Task MoveTo(Vector3 nextPosition, 
            float rotationDuration = 0.5f,
            float movementDuration = 0.5f,
            Func<Task> beforeMoving = null, 
            Func<Task> afterMoving = null)
        {
            var prevRotation = transform.localEulerAngles;
            await LookTo(nextPosition, rotationDuration);
            if (beforeMoving != null)
                await beforeMoving();
            await MoveToWithoutRotating(nextPosition, movementDuration);
            if (afterMoving!= null)
                await afterMoving();
            await Rotate(prevRotation, rotationDuration);
        }
    }
}