using System;

namespace checkers
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class BoardAttribute : Attribute
    { }
}