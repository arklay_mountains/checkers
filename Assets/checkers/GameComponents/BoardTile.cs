using System;
using UnityEngine;

namespace checkers
{
    [RequireComponent(typeof(GameComponentSelectable))]
    public class BoardTile : GameComponent
    {
        GameComponentSelectable _linkedGameComponentSelectable;
        public GameComponentSelectable selectable => _linkedGameComponentSelectable = _linkedGameComponentSelectable ? _linkedGameComponentSelectable : GetComponent<GameComponentSelectable>();
    }
}