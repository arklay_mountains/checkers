using UnityEngine;

namespace checkers
{
    public class BoardTilesBag : Bag<BoardTile>
    {
        public BoardTile GetTile(GameController game)
        {
            var ret = ActivateObject();
            ret.Setup(game);
            return ret;
        }        
    }
}