using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace checkers
{
    public class Ragdoll: MonoBehaviour
    {
        Rigidbody[] _linkedRigidbodies;
        Rigidbody[] linkedRigidbodies => _linkedRigidbodies = _linkedRigidbodies != null ? _linkedRigidbodies : GetComponentsInChildren<Rigidbody>();

        public float forceMultiplier = 20;
        public Task Die(Vector3 killerPosition)
        {
            BecomeRagdoll(true);

            var force = transform.position - killerPosition;
            
            var impactTarget = linkedRigidbodies[Random.Range(0, linkedRigidbodies.Length)];
            impactTarget.AddForce(force * forceMultiplier, ForceMode.VelocityChange);
            return Task.CompletedTask;
        }
                
        void BecomeRagdoll(bool ragdoll)
        {
            if(ragdoll)
                Unfreeze();
            else
                Freeze();
            
            GetComponent<Animator>().enabled = !ragdoll;
        }

        public void Revive()
        {
            BecomeRagdoll(false);
        }

        public void Freeze()
        {
            foreach (var rb in linkedRigidbodies)
            {
                rb.isKinematic = true;
            }
        }
        void Unfreeze()
        {
            foreach (var rb in linkedRigidbodies)
            {
                rb.isKinematic = false;
            }
        }

        [Button(), HideInPlayMode]
        void AddRagdollComponents()
        {
            RigCharacter(transform.Find("Bip001"));
            var human = transform.Find("Bip001/Bip001 Pelvis/Bip001 Spine/Bip002");
            if (human != null)
            {
                RigCharacter(human);
            }
        }

        void RigCharacter(Transform human)
        {
            string suffix = "";
            string pelvisName = "Bip001 Pelvis";
            var pelvis = human.Find(pelvisName);
            if (pelvis == null)
            {
                suffix = "001";
                pelvis = human.Find(pelvisName + suffix);
            }

            var pelvisRigidbody = RigPelvis(pelvis, 3.1f, 
                new Vector3(-0.1545011f, 0.03208652f, -0.001890585f),
                new Vector3(0.3211208f, 0.3512643f, 0.2840681f));

            var legsParent = pelvis.Find($"Bip001 Spine{suffix}");
            var leg = legsParent.Find($"Bip001 L Thigh{suffix}");
            if (leg == null)
            {
                legsParent = pelvis;
            }
            RigLeg(legsParent.Find($"Bip001 L Thigh{suffix}"), suffix, "L", pelvisRigidbody);
            RigLeg(legsParent.Find($"Bip001 R Thigh{suffix}"), suffix, "R", pelvisRigidbody);

            RigHead(pelvis.Find($"Bip001 Spine{suffix}/Bip001 Neck{suffix}/Bip001 Head{suffix}"), pelvisRigidbody, 0.15f);

            var armsParent = pelvis.Find($"Bip001 Spine{suffix}/Bip001 Neck{suffix}");
            var arm = armsParent.Find($"Bip001 L Clavicle{suffix}/Bip001 L UpperArm{suffix}");
            if (arm == null)
            {
                armsParent = pelvis.Find($"Bip001 Spine{suffix}");;
            }
            RigArm(armsParent.Find($"Bip001 L Clavicle{suffix}/Bip001 L UpperArm{suffix}"), suffix, "L", pelvisRigidbody);
            RigArm(armsParent.Find($"Bip001 R Clavicle{suffix}/Bip001 R UpperArm{suffix}"), suffix, "R", pelvisRigidbody);
        }

        static Rigidbody RigPelvis(Transform pelvis, 
            float rigidbodyMass, Vector3 colliderCenter, Vector3 colliderSize)
        {
            var rigidbody = pelvis.gameObject.EnsureComponent<Rigidbody>();
            rigidbody.mass = 3.1f;
            rigidbody.isKinematic = true;

            var collider = pelvis.gameObject.EnsureComponent<BoxCollider>();
            collider.center = new Vector3(-0.1545011f, 0.03208652f, -0.001890585f);
            collider.size = new Vector3(0.3211208f, 0.3512643f, 0.2840681f);
            return rigidbody;
        }

        void RigHead(Transform head, Rigidbody pelvis, float radius)
        {
            var rigidbody = head.gameObject.EnsureComponent<Rigidbody>();
            rigidbody.isKinematic = true;
            var collider = head.gameObject.EnsureComponent<SphereCollider>();
            collider.radius = radius;
            var joint = head.gameObject.EnsureComponent<CharacterJoint>();
            joint.connectedBody = pelvis;
        }

        static Rigidbody Rig(Transform bone, Rigidbody parent, 
            Vector3 colliderCenter, float colliderRadius, float colliderHeight, int colliderDirection)
        {
            var rigidbody = bone.gameObject.EnsureComponent<Rigidbody>();
            rigidbody.isKinematic = true;
            var collider = bone.gameObject.EnsureComponent<CapsuleCollider>();
            collider.center = colliderCenter;
            collider.radius = colliderRadius;
            collider.height = colliderHeight;
            collider.direction = colliderDirection;
            var joint = bone.gameObject.EnsureComponent<CharacterJoint>();
            joint.connectedBody = parent;

            return rigidbody;
        }

        void RigLeg(Transform leg, string suffix, string side, Rigidbody pelvis)
        {
            var rigidbody = Rig(leg, pelvis, 
                new Vector3(-0.15f, 0f, 0f), 0.09f, 0.37f, 0);
            RigCalf(leg.Find($"Bip001 {side} Calf" + suffix), suffix, side, rigidbody);
        }

        void RigCalf(Transform calf, string suffix, string side, Rigidbody leg)
        {
            var rigidbody = Rig(calf, leg, 
                new Vector3(-0.1f, 0f, 0f), 0.09f, 0.37f, 0);
            RigFoot(calf.Find($"Bip001 {side} Foot" + suffix), rigidbody);
        }

        void RigFoot(Transform foot, Rigidbody calf)
        {
            Rig(foot, calf, 
                new Vector3(-0.04f, 0.09f, 0f), 0.05f, 0.37f, 1);
        }

        void RigArm(Transform arm, string suffix, string side, Rigidbody pelvis)
        {
            var rigidbody = Rig(arm, pelvis, 
                new Vector3(-0.15f, 0f, 0f), 0.1f, 0.5f, 0);
            RigForearm(arm.Find($"Bip001 {side} Forearm" + suffix), rigidbody);
        }

        void RigForearm(Transform forearm, Rigidbody arm)
        {
            Rig(forearm, arm, 
                new Vector3(-0.15f, 0f, 0f), 0.1f, 0.5f, 0);
        }
    }
}