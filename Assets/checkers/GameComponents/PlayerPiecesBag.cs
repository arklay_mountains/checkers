namespace checkers
{
    public class PlayerPiecesBag : Bag<PlayerPiece>
    {
        public PlayerPiece GetPiece(GameController game)
        {
            var ret = ActivateObject();
            ret.Setup(game);
            return ret;
        }

        public void ReturnPiece(PlayerPiece piece)
        {
            DeactivateObject(piece);
        }
    }
}