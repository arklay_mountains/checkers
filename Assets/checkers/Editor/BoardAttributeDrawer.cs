#if UNITY_EDITOR
using System;
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEngine;

namespace checkers
{
    public sealed class BoardAttributeDrawer : OdinAttributeDrawer<BoardAttribute, Tile[,]>
    {
        const float tileSize = 40;

        protected override void DrawPropertyLayout(GUIContent label)
        {
            var game = (GameController)Property.ParentValues[0];
            var tiles = ValueEntry.SmartValue;
            Rect rect = EditorGUILayout.GetControlRect(false, tileSize * 8 + 20);
            rect = rect.AlignCenter(tileSize * 8);

            rect = rect.AlignBottom(rect.height);
            SirenixEditorGUI.DrawSolidRect(rect, new Color(0.7f, 0.7f, 0.7f, 1f));

            for (int yIndex = 0; yIndex < 8; yIndex++)
            {
                for (int xIndex = 0; xIndex < 8; xIndex++)
                {
                    var tile = tiles[xIndex, yIndex];
                    if (tile == null)
                        continue;
                    
                    Rect tileRect = rect.SplitGrid(tileSize, tileSize, (7 - yIndex) * 8 + xIndex);
                    SirenixEditorGUI.DrawBorders(
                        tileRect.SetWidth(tileRect.width + 1).SetHeight(tileRect.height + 1),
                        1);
				
                    SirenixEditorGUI.DrawSolidRect(
                        new Rect(tileRect.x + 1, tileRect.y + 1, tileRect.width - 1, tileRect.height - 1),
                        (xIndex % 2 == 0) ^ (yIndex % 2 == 0) ? Color.red : Color.gray);

                    if (!tile.isEmpty)
                    {
                        GUIHelper.PushColor(tile.playerColor == TPlayerColor.black ? Color.black : Color.white);
                        GUI.Label(tileRect.AlignCenter(18).AlignMiddle(18), tile.isQueen ?  EditorIcons.Flag.ActiveGUIContent : EditorIcons.AlertCircle.ActiveGUIContent);
                        GUIHelper.PopColor();
                    }

                    if (!((xIndex % 2 == 0) ^ (yIndex % 2 == 0)))
                    {
                        if (tileRect.Contains(Event.current.mousePosition))
                        {
                            SirenixEditorGUI.DrawSolidRect(
                                new Rect(tileRect.x + 1, tileRect.y + 1, tileRect.width - 1, tileRect.height - 1),
                                new Color(0f, 1f, 0f, 0.3f));

                            if (Event.current.type == EventType.MouseDown)
                            {
                                var currentState = tiles[xIndex, yIndex].CurrentState();
                                if (Event.current.button == 0)
                                {
                                    tiles[xIndex, yIndex].ChangeState(game, (Tile.TState)(currentState + 1),
                                        new Vector2Int(xIndex, yIndex));
                                    Event.current.Use();
                                }
                                else if (Event.current.button == 1)
                                {
                                    tiles[xIndex, yIndex].ChangeState(game, (Tile.TState)(currentState - 1),
                                        new Vector2Int(xIndex, yIndex));
                                    Event.current.Use();
                                }

                                game.ResetComponents();
                                game.NextTurn(TPlayerColor.red, true);
                            }
                        }
                    }

                    if (xIndex == 0)
                    {
                        GUIHelper.PushColor(Color.black);
                        GUI.Label(tileRect.AlignLeft(18).AlignMiddle(18), yIndex.ToString());
                        GUIHelper.PopColor();
                    }
                    if (yIndex == 0)
                    {
                        GUIHelper.PushColor(Color.black);
                        GUI.Label(tileRect.AlignCenter(18).AlignBottom(18), xIndex.ToString());
                        GUIHelper.PopColor();
                    }
                }
            }

            if (GUI.Button(rect.AlignLeft(100).AlignBottom(20), "Red turn"))
            {
                game.ResetComponents();
                game.NextTurn(TPlayerColor.red, true);
            }
            if (GUI.Button(rect.AlignRight(100).AlignBottom(20), "Black turn"))
            {
                game.ResetComponents();
                game.NextTurn(TPlayerColor.black, true);
            }

            GUIHelper.RequestRepaint();

        }
    }
}
#endif