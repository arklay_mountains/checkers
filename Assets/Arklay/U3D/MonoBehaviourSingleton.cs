﻿using UnityEngine;

public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviourSingleton<T>
{
    protected static bool applicationQuitting = false;
    protected virtual void OnApplicationQuit()
    {
        applicationQuitting = true;
    }

    static T __instance = null;
    public static T instance
    {
        get
        {
            if (applicationQuitting || __instance) return __instance;

            Object[] objs = Resources.FindObjectsOfTypeAll(typeof(T));
            if (objs.Length > 0)
            {
                __instance = objs[0] as T;
            }
            if (__instance == null)
            {
                __instance = new GameObject(typeof(T).FullName).AddComponent<T>();
            }
            Debug.Assert(__instance != null, "Cannot create singleton for " + typeof(T).Name);
            return __instance;
        }
        private set
        {
            if (__instance == value)
                return;
            if (__instance != null)
            {
                Destroy(value.gameObject);
                throw new System.InvalidOperationException(string.Format("Two singletons ({0} - {1}) in the scene!", __instance.gameObject.name, value.gameObject.name));
            }
            __instance = value;
        }
    }
    public virtual void Awake()
    {
        instance = (T)this;
    }

    public virtual void OnDestroy()
    {
        __instance = null;
    }
}
