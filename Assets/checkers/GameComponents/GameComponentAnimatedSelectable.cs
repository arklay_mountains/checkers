using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace checkers
{
    [RequireComponent(typeof(GameComponentAnimation))]
    [RequireComponent(typeof(GameComponentSelectable))]
    [DisallowMultipleComponent]
    public class GameComponentAnimatedSelectable : MonoBehaviour
    {
        GameComponentSelectable _linkedGameComponentSelectable;
        GameComponentSelectable linkedGameComponentSelectable => _linkedGameComponentSelectable = _linkedGameComponentSelectable ? _linkedGameComponentSelectable : GetComponent<GameComponentSelectable>();
        GameComponentAnimation _linkedGameComponentAnimation;
        GameComponentAnimation linkedGameComponentAnimation => _linkedGameComponentAnimation = _linkedGameComponentAnimation ? _linkedGameComponentAnimation : GetComponent<GameComponentAnimation>();
        GameComponent _linkedGameComponent;
        GameComponent linkedGameComponent => _linkedGameComponent = _linkedGameComponent ? _linkedGameComponent : GetComponent<GameComponent>();


        void Start()
        {
            linkedGameComponentSelectable.onSelectedChanged += Selected;
            linkedGameComponentSelectable.onSelectableChanged += Selectable;
            
            Selected(linkedGameComponentSelectable.selected);
            Selectable(linkedGameComponentSelectable.selectable);
        }

        void Selectable(bool selectable)
        {
            if(!linkedGameComponentSelectable.selected)
                linkedGameComponentAnimation.animator.SetBool(nameof(selectable), selectable);
        }

        void Selected(bool selected)
        {
            linkedGameComponentAnimation.animator.SetBool(nameof(selected), selected);
            Selectable(false);
        }
    }
}