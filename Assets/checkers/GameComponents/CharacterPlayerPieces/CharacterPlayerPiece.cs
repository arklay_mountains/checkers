using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using U3D.Animators;
using UnityEngine;

namespace checkers
{
    [RequireComponent(typeof(GameComponentAnimation))]
    [RequireComponent(typeof(GameComponentMoveable))]
    [RequireComponent(typeof(GameComponentAnimatedSelectable))]
    public class CharacterPlayerPiece : PlayerPiece
    {
        GameComponentMoveable _linkedGameComponentMoveable;
        GameComponentMoveable linkedGameComponentMoveable => _linkedGameComponentMoveable = _linkedGameComponentMoveable ? _linkedGameComponentMoveable : GetComponent<GameComponentMoveable>();
        GameComponentAnimation _linkedGameComponentAnimation;
        GameComponentAnimation linkedGameComponentAnimation => _linkedGameComponentAnimation = _linkedGameComponentAnimation ? _linkedGameComponentAnimation : GetComponent<GameComponentAnimation>();
        
        enum TCharacterAnimations
        {
            Kill, Walk, Idle
        }
        public GameComponentAnimation characterAnimator;
        public Transform charactersContainer;
        Ragdoll _characterRagdoll;
        Ragdoll characterRagdoll => _characterRagdoll = _characterRagdoll ? _characterRagdoll : characterAnimator.GetComponent<Ragdoll>();

        static Dictionary<CharacterPlayerPiece, Dictionary<string, int>> allUsages = new Dictionary<CharacterPlayerPiece, Dictionary<string, int>>();
        Dictionary<string, int> usages
        {
            get
            {
                if (!allUsages.ContainsKey(this))
                {
                    allUsages.Add(this, new Dictionary<string, int>());
                    foreach (Transform child in charactersContainer)
                    {
                        allUsages[this].Add(child.name, 0);
                    }
                }

                return allUsages[this];
            }
        }

        string characterName;
        void OnEnable()
        {
            if (characterAnimator.transform.childCount == 0)
            {
                int minAppareances = int.MaxValue;
                foreach (var usagesValue in usages.Values)
                {
                    minAppareances = Math.Min(usagesValue, minAppareances);
                }

                var possibleChoices = new List<string>();
                foreach (var usage in usages)
                {
                    if (usage.Value == minAppareances)
                    {
                        possibleChoices.Add(usage.Key);
                    }
                }

                var choice = possibleChoices[UnityEngine.Random.Range(0, possibleChoices.Count)];
                characterName = choice;
                usages[choice]++;

                var selectedCharacter = charactersContainer.Find(choice);
                while (selectedCharacter.childCount > 0)
                {
                    var child = selectedCharacter.GetChild(0);
                    child.SetParent(characterAnimator.transform, false);
                }
                DestroyImmediate(selectedCharacter.gameObject);
            }

            var partsVariation = characterAnimator.GetComponent<CharacterPartsVariations>();
            if (partsVariation != null)
            {
                partsVariation.Randomize();
            }
        }

        void OnDisable()
        {
            var characterGO= new GameObject(characterName);
            characterGO.transform.SetParent(charactersContainer);
            characterGO.transform.localPosition = Vector3.zero;
            characterGO.transform.localEulerAngles= Vector3.zero;

            while (characterAnimator.transform.childCount > 0)
            {
                var child = characterAnimator.transform.GetChild(0);
                child.SetParent(characterGO.transform);
            }
            
            characterName = null;
        }

        enum TAnimations
        {
            BackToIdle, Die, Dead, BecomeQueen
        }
        
        public float walkDuration = 1f;
        public AudioSource walkSound;
        public override async Task MoveTo(Vector2Int to)
        {
            await linkedGameComponentMoveable.MoveTo(game.GetPositionForCoords(to),
                movementDuration: walkDuration,
                beforeMoving:() =>
                {
                    characterAnimator.RaiseTrigger(TCharacterAnimations.Walk);
                    walkSound.Play();
                    return Task.CompletedTask;
                },
                afterMoving: () =>
                {
                    walkSound.Stop();
                    walkSound.time = 0;
                    characterAnimator.RaiseTrigger(TCharacterAnimations.Idle);
                    linkedGameComponentAnimation.RaiseTrigger(TAnimations.BackToIdle);
                    return Task.CompletedTask;
                });
        }

        public override async Task Kill(Vector2Int killCoords, Vector2Int to)
        {
            await linkedGameComponentMoveable.MoveTo(game.GetPositionForCoords(to),
                movementDuration: walkDuration * 2f,
                beforeMoving: KillAndWalk,
                afterMoving: () =>
                {
                    walkSound.Stop();
                    walkSound.time = 0;
                    characterAnimator.RaiseTrigger(TCharacterAnimations.Idle);
                    return Task.CompletedTask;
                });
        }

        public float killWaitSeconds = 1;
        public AudioSource killSound;
        async Task KillAndWalk()
        {
            await Task.Delay(TimeSpan.FromSeconds(killWaitSeconds));
            killSound.Play();
            await characterAnimator.PlayAnimation(TCharacterAnimations.Kill);
            killSound.Stop();
            killSound.time = 0;
            characterAnimator.RaiseTrigger(TCharacterAnimations.Walk);
            walkSound.Play();
        }

        [Button("Test idle"), HideInEditorMode]
        void TestIdle()
        {
            Idle();
        }
        [Button("Die"), HideInEditorMode]
        void TestDie()
        {
            Die(new Vector3(-1, 0, -1)).ConfigureAwait(true);
        }
        public override async Task Die(Vector2Int killerCoords)
        {
            await Die(game.GetPositionForCoords(killerCoords));
        }

        float originalRotation;
        public AudioSource dieSound;

        async Task Die(Vector3 killerPosition)
        {
            originalRotation = transform.localEulerAngles.y;

            await linkedGameComponentAnimation.PlayAnimation(TAnimations.Die);
            await linkedGameComponentMoveable.LookTo(killerPosition, 0.5f);
            await characterRagdoll.Die(killerPosition);

            dieSound.Play();
            await Task.Delay(TimeSpan.FromSeconds(2));
            dieSound.Stop();
            dieSound.time = 0;
            
            characterRagdoll.Freeze();
            linkedGameComponentAnimation.RaiseTrigger(TAnimations.Dead);
        }

        [Button("Revive"), HideInEditorMode]
        public override void Revive()
        {
            transform.localEulerAngles = Vector3.up * originalRotation;
            characterRagdoll.Revive();
        }

        [Button("Become queen"), HideInEditorMode]
        void TestBecomeQueen()
        {
            BecomeQueen().ConfigureAwait(true);
        }
        public AudioSource becomeQueenSound;
        public override async Task BecomeQueen()
        {
            becomeQueenSound.Play();
            await linkedGameComponentAnimation.PlayAnimation(TAnimations.BecomeQueen);
            becomeQueenSound.Stop();
            becomeQueenSound.time = 0;
        }
    }
}