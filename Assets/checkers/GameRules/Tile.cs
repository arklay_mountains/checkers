using System;
using UnityEngine;

namespace checkers
{
    public partial class Tile
    {
        public readonly BoardTile boardTile;
        public PlayerPiece playerPiece { get; private set; }

        TPlayerColor? _playerColor;
        bool? _isQueen;
        public Tile(BoardTile boardTile, PlayerPiece playerPiece, TPlayerColor? playerColor)
        {
            this.boardTile = boardTile;

            this.playerPiece = playerPiece;
            _playerColor = playerColor;
            _isQueen = playerPiece == null ? null : (bool?) false;
            _states = null;
        }

        public bool isEmpty => playerPiece == null;

        public TPlayerColor playerColor => _playerColor.Value;
        public bool isQueen => _isQueen.Value;

        public void MoveFrom(Tile tile)
        {
            SetPiece(tile.playerPiece, tile.playerColor, tile.isQueen);
            tile.MakeEmpty();
        }

        public void SetPiece(PlayerPiece piece, TPlayerColor color, bool isQueen)
        {
            playerPiece = piece;
            _playerColor = color;
            _isQueen = isQueen;
        }
        
        public void MakeEmpty()
        {
            playerPiece = null;
            _playerColor = null;
            _isQueen = null;
        }

        public enum TState
        {
            empty,
            red,
            black,
            redQueen,
            blackQueen
        }

        public TState CurrentState()
        {
            if (isEmpty)
                return TState.empty;
            if (!isQueen)
            {
                if (playerColor == TPlayerColor.red)
                    return TState.red;
                return TState.black;
            }

            if (playerColor == TPlayerColor.red)
                return TState.redQueen;
            return TState.blackQueen;
        }
        public void SetState(GameController game, TState s)
        {
            switch (s)
            {
                case TState.empty:
                    MakeEmpty(game);
                    break;
                case TState.red:
                    MakeRedPiece(game);
                    break;                
                case TState.redQueen:
                    MakeRedQueenPiece(game);
                    break;                
                case TState.black:
                    MakeBlackPiece(game);
                    break;                
                case TState.blackQueen:
                    MakeBlackQueenPiece(game);
                    break;                
            }
        }

    }
}