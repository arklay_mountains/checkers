using UnityEngine;

namespace checkers
{
    [DisallowMultipleComponent]
    public abstract class Bag<T> : MonoBehaviourPool<T> where T : GameComponent
    {
        public void Clear()
        {
            DeactivateAllObjects();
        }
    }
}