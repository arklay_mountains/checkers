using UnityEngine;

public static class CollidersExtensions
{
    /// <summary>
    /// Calculates the closest point of a ray to a collider. 
    /// </summary>
    /// <param name="me">Target collider</param>
    /// <param name="ray">Target ray</param>
    /// <param name="length">Length of the ray</param>
    /// <param name="maxIterations">Max iterations to find the point</param>
    /// <returns></returns>
    public static Vector3 ClosestPointToRay(this Collider me, Ray ray, float length, int maxIterations = 10)
    {
        Vector3 ret = Vector3.zero;
        float currentFactor = 0f;

        for (int i = 0; i < maxIterations; i++)
        {
            var currentPoint = ray.origin + ray.direction * currentFactor;
            var currentPointOnBounds =  me.ClosestPointOnBounds(currentPoint);
            var currentDistance = (currentPoint - currentPointOnBounds).magnitude;

            if (currentDistance == 0 || currentFactor == 1)
            {
                return currentPointOnBounds;
            }
            else
            {
                ret = currentPointOnBounds;
                var currentPointOnBoundsDistanceToOrigin = (ray.origin - currentPointOnBounds).magnitude;
                currentFactor = Mathf.Min(1, currentPointOnBoundsDistanceToOrigin / length);
            }
        }

        return ret;
    }
}
