using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using U3D.Animators;
using UnityEngine;

namespace checkers
{
    [RequireComponent(typeof(GameComponentAnimation))]
    [RequireComponent(typeof(GameComponentMoveable))]
    [RequireComponent(typeof(GameComponentAnimatedSelectable))]
    public class BasicPlayerPiece : PlayerPiece
    {
        GameComponentMoveable _linkedGameComponentMoveable;
        GameComponentMoveable linkedGameComponentMoveable => _linkedGameComponentMoveable = _linkedGameComponentMoveable ? _linkedGameComponentMoveable : GetComponent<GameComponentMoveable>();        
        GameComponentAnimation _linkedGameComponentAnimation;
        GameComponentAnimation linkedGameComponentAnimation => _linkedGameComponentAnimation = _linkedGameComponentAnimation ? _linkedGameComponentAnimation : GetComponent<GameComponentAnimation>();
        enum TAnimations
        {
            Die, BecomeQueen
        }

        public override Task MoveTo(Vector2Int to)
        {
            return linkedGameComponentMoveable.MoveTo(game.GetPositionForCoords(to));
        }
        public override Task Kill(Vector2Int killCoords, Vector2Int to)
        {
            var tcs = new TaskCompletionSource<object>();
            var killPosition = game.GetPositionForCoords(killCoords) + Vector3.up * 2;
            
            var deltaMove = to - this.coords;
            var prevRotation = transform.localEulerAngles;
            var nextRotation = -Vector2.SignedAngle(Vector2.up, deltaMove);
            var nextPosition = game.GetPositionForCoords(to);
            
            var moveSequence = DOTween.Sequence();
            moveSequence
                .Append(transform.DOLocalRotate(Vector3.up * nextRotation, 0.5f))
                .Append(transform.DOLocalMove(killPosition, 0.5f))
                .Append(transform.DOLocalMove(nextPosition, 0.5f))
                .Append(transform.DOLocalRotate(prevRotation, 0.5f))
                .OnComplete(() => { tcs.SetResult(null); });

            return tcs.Task;
        }
        public override Task Die(Vector2Int killerCoords)
        {
            return Task.CompletedTask;
        }
        public override async Task BecomeQueen()
        {
            await linkedGameComponentAnimation.PlayAnimation(TAnimations.BecomeQueen);
        }
    }
}