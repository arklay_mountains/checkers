using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace checkers
{
    public class CharacterPartsVariations : MonoBehaviour
    {
        [Button("Randomize")]
        public void Randomize()
        {
            var variations = new Dictionary<string, List<Transform>>();

            foreach (Transform child in this.transform)
            {
                var fixedPart = child.name.Remove(child.name.Length - 1);
                if (fixedPart.EndsWith("_"))
                {
                    if (!variations.ContainsKey(fixedPart))
                    {
                        variations.Add(fixedPart, new List<Transform>());
                    }

                    variations[fixedPart].Add(child);
                    child.gameObject.SetActive(false);
                }
            }

            foreach (var variation in variations.Values)
            {
                var selectedIndex = Random.Range(0, variation.Count);
                variation[selectedIndex].gameObject.SetActive(true);
            }
        }
    }
}
