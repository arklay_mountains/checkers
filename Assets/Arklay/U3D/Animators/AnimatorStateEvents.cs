using System;
using UnityEngine;
using UnityEngine.Animations;

namespace U3D.Animators
{
    public class AnimatorStateEvents : StateMachineBehaviour
    {
        public event Action<Animator, AnimatorStateInfo, int> onStateEnter, onStateExit;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, animatorStateInfo, layerIndex);
            onStateEnter?.Invoke(animator, animatorStateInfo, layerIndex);
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            base.OnStateExit(animator, animatorStateInfo, layerIndex);
            onStateExit?.Invoke(animator, animatorStateInfo, layerIndex);
        }
    }
}