using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace checkers
{
    public partial class GameController
    {
        void SetupTiles()
        {
            redTilesBag.Clear();
            blackTilesBag.Clear();
            redPlayerPiecesBag.Clear();
            blackPlayerPiecesBag.Clear();
            redPlayerQueenPiecesBag.Clear();
            blackPlayerQueenPiecesBag.Clear();
			
            for (int yIndex = 0; yIndex < 8; yIndex++)
            {
                for (int xIndex = 0; xIndex < 8; xIndex++)
                {
                    BoardTile boardTile;
                    if ((xIndex % 2 == 0) ^ (yIndex % 2 == 0))
                    {
                        boardTile = redTilesBag.GetTile(this);
                    }
                    else
                    {
                        boardTile = blackTilesBag.GetTile(this);
                    }

                    PlayerPiece piece = null;
                    TPlayerColor? playerColor = null;
                    if (yIndex < 3)
                    {
                        if (xIndex % 2 == yIndex % 2)
                        {
                            playerColor = TPlayerColor.red;
                            piece = redPlayerPiecesBag.GetPiece(this);
                        }
                    }
                    if (yIndex > 4)
                    {
                        if (xIndex % 2 == yIndex % 2)
                        {
                            playerColor = TPlayerColor.black;
                            piece = blackPlayerPiecesBag.GetPiece(this);
                        }
                    }

                    tiles[xIndex, yIndex] = new Tile(boardTile, piece, playerColor);
                    
                    boardTile.Idle();
                    if (piece != null)
                    {
                        piece.Idle();
                    }
                }
            }
        }
        void CleanupTiles()
        {
            redTilesBag.Clear();
            blackTilesBag.Clear();
            redPlayerPiecesBag.Clear();
            blackPlayerPiecesBag.Clear();
            redPlayerQueenPiecesBag.Clear();
            blackPlayerQueenPiecesBag.Clear();
			
            for (int yIndex = 0; yIndex < 8; yIndex++)
            {
                for (int xIndex = 0; xIndex < 8; xIndex++)
                {
                    tiles[xIndex, yIndex] = null;
                }
            }
        }
        
        public void ReturnPiece(Tile t)
        {
            if (t.playerPiece != null)
            {
                if (t.isQueen)
                {
                    (t.playerColor == TPlayerColor.red ? redPlayerQueenPiecesBag : blackPlayerQueenPiecesBag)
                        .ReturnPiece(t.playerPiece);
                }
                else
                {
                    (t.playerColor == TPlayerColor.red ? redPlayerPiecesBag : blackPlayerPiecesBag).ReturnPiece(
                        t.playerPiece);
                }
            }
        }
    }
}