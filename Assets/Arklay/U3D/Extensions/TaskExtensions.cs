﻿using System;
using System.Threading.Tasks;
using U3D.Threading;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public static class TaskExtensions
{
    static Task ContinueInMainThreadWith(this Task t, Action<Task> a)
    {
        var tcs = new TaskCompletionSource<object>();
        t.ContinueWith((r) =>
        {
            Dispatcher.instance.ToMainThread(() =>
            {
                try
                {
                    a(r);
                    tcs.SetResult(null);
                }
                catch(Exception e)
                {
                    tcs.SetException(e);
                }
            });
        });
        return tcs.Task;
    }

    static Task ContinueInMainThreadWith<T>(this Task<T> t, Action<Task<T>> a)
    {
        var tcs = new TaskCompletionSource<object>();
        t.ContinueWith((r) =>
        {
            Dispatcher.instance.ToMainThread(() =>
            {
                try
                {
                    a(r);
                    tcs.SetResult(null);
                }
                catch (Exception e)
                {
                    tcs.SetException(e);
                }
            });
        });
        return tcs.Task;
    }
}
