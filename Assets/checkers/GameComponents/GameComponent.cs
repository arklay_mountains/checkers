using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace checkers
{
    [DisallowMultipleComponent]
    public abstract class GameComponent : MonoBehaviour
    {
        public Vector2Int coords => game.GetCoordsFor(this);
        
        protected GameController game;
        public void Setup(GameController game)
        {
            this.game = game;
        }

        public event Action onIdle;
        public void Idle()
        {
            transform.localPosition = game == null ? Vector3.zero : game.GetPositionForCoords(coords);
            onIdle?.Invoke();
        }
    }
}