﻿using UnityEngine;
using Vuforia;

public static class VectorExtensions
{
    public static float DistanceToRay(this Vector3 point, Ray ray)
    {
        return Vector3.Cross(ray.direction, point - ray.origin).magnitude;
    }
}
