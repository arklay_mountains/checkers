using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;

namespace checkers
{
    public enum TPlayerColor {red, black}

    // http://www.wcdf.net/rules/rules_of_checkers_english.pdf
    public partial class GameController
    {
        [ShowInInspector]
        [HideInEditorMode, ShowIf(nameof(playing))]
        [Board]
        readonly Tile[,] tiles= new Tile[8,8];

        public List<Vector2Int> PossibleMoves(Vector2Int coords)
        {
            bool _;
            return PossibleMoves(coords, out _);
        }
        List<Vector2Int> PossibleMoves(Vector2Int coords, out bool isKillingMove)
        {
            var possibleMoves = AllPossibleMoves(coords);
            var normalMoves = new List<Vector2Int>();
            var killingMoves = new List<Vector2Int>();

            var oppsitePlayer = GetTileFor(coords).playerColor == TPlayerColor.red ? TPlayerColor.black : TPlayerColor.red;
            foreach (var possibleMove in possibleMoves)
            {
                var tile = tiles[possibleMove.x, possibleMove.y];
                if (tile.isEmpty)
                {
                    normalMoves.Add(possibleMove);
                }
                else if (tile.playerColor == oppsitePlayer)
                {
                    var deltaMove = possibleMove - coords;
                    var nextMove = coords + deltaMove * 2;
                    if (nextMove.x >= 0 && nextMove.y >= 0 && nextMove.x <= 7 && nextMove.y <= 7)
                    {
                        if (tiles[nextMove.x, nextMove.y].isEmpty)
                        {
                            killingMoves.Add(nextMove);
                        }
                    }
                }
            }

            isKillingMove = killingMoves.Count > 0;
            return isKillingMove ? killingMoves : normalMoves;            
        }
        List<Vector2Int> AllPossibleMoves(Vector2Int coords)
        {
            var tile = GetTileFor(coords);
            List<Vector2Int> ret = new List<Vector2Int>();
            
            int[] possibleYValues;
            if (tile.isQueen)
                possibleYValues = new [] { -1, +1 };
            else
                possibleYValues = new [] { tile.playerColor == TPlayerColor.red ? +1 : -1 };

            foreach (var possibleYValue in possibleYValues)
            {
                var moveToY = coords.y + possibleYValue;
                if (moveToY >= 0 && moveToY <= 7)
                {
                    if (coords.x > 0)
                    {
                        ret.Add(new Vector2Int(coords.x - 1, moveToY));
                    }

                    if (coords.x < 7)
                    {
                        ret.Add(new Vector2Int(coords.x + 1, moveToY));
                    }
                }
            }

            return ret;
        }
        
                
        public async Task ProcessMove(Vector2Int from, Vector2Int to)
        {
            var delta = to - from;
            var tileMovingTo = tiles[to.x, to.y];
            var playerColor = tiles[from.x, from.y].playerColor;

            if (!tiles[from.x, from.y].isQueen &&
                (playerColor == TPlayerColor.red && to.y == 7 ||
                playerColor == TPlayerColor.black && to.y == 0))
            {    // became queen
                drawableTurns = 0;
                
                if (Mathf.Abs(delta.x) > 1)
                {    // and killed something
                    await ProcessKilling(from, to);
                }
                else
                {
                    await ProcessSimpleMove(from, to);
                }

                await tileMovingTo.playerPiece.BecomeQueen();

                var originalPiece = tileMovingTo.playerPiece;
                ReturnPiece(tileMovingTo);

                PlayerPiece queenPiece;
                if (playerColor == TPlayerColor.red)
                {
                    queenPiece = redPlayerQueenPiecesBag.GetPiece(this);
                }
                else
                {
                    queenPiece = blackPlayerQueenPiecesBag.GetPiece(this);
                }

                tileMovingTo.SetPiece(queenPiece, playerColor, true);
                queenPiece.Idle();
                
                NextTurn(OppositeColor(playerColor));
            }
            else if (Mathf.Abs(delta.x) > 1)
            {    // killed something
                await ProcessKilling(from, to);

                bool moreKillingPosible;
                PossibleMoves(to, out moreKillingPosible);

                if (moreKillingPosible)
                {
                    currentState = new ChooseTile(currentState, tileMovingTo.playerPiece);
                }
                else
                {
                    tileMovingTo.playerPiece.Idle();
                    NextTurn(OppositeColor(playerColor));
                }
            }
            else
            {
                if (!tiles[from.x, from.y].isQueen)
                {
                    drawableTurns = 0;
                }

                // nothing special
                await ProcessSimpleMove(from, to);
                tileMovingTo.playerPiece.Idle();
                NextTurn(OppositeColor(playerColor));
            }
        }

        async Task ProcessSimpleMove(Vector2Int from, Vector2Int to)
        {
            await tiles[from.x, from.y].playerPiece.MoveTo(to);
            tiles[to.x, to.y].MoveFrom(tiles[from.x, from.y]);
        }

        async Task ProcessKilling(Vector2Int from, Vector2Int to)
        {
            drawableTurns++;
            var delta = to - from;
            var movingFrom = tiles[from.x, from.y];
            var movingTo = tiles[to.x, to.y];

            var killedPosition = @from + new Vector2Int(delta.x / 2, delta.y / 2);
            var killedTile = tiles[killedPosition.x, killedPosition.y];

            var fightTasks = new Task[]
            {
                killedTile.playerPiece.Die(@from),
                movingFrom.playerPiece.Kill(killedPosition, to)
            };

            await Task.WhenAll(fightTasks);

            movingTo.MoveFrom(movingFrom);
            ReturnPiece(killedTile);
            killedTile.playerPiece.Revive();
            killedTile.MakeEmpty();
        }
        
        readonly Dictionary<string, int> pastBoardPositions = new Dictionary<string, int>();
        int drawableTurns;
        public void NextTurn(TPlayerColor nextTurnPlayer, bool debug = false)
        {
            if (!debug)
            {
                var currentBoardState = GetCurrentBoardPositions();
                if (!pastBoardPositions.ContainsKey(currentBoardState))
                    pastBoardPositions.Add(currentBoardState, 0);
                pastBoardPositions[currentBoardState]++;

                if (pastBoardPositions[currentBoardState] >= 3)
                {
                    currentState = new Draw(this, uiTexts.drawReason3TimesSamePositions);
                    return;
                }

                drawableTurns++;
                if (drawableTurns >= 40)
                {
                    currentState = new Draw(this, uiTexts.drawReason40turnsWithoutForwardMoveByNotQueenNorKills);
                    return;
                }

                SaveState(currentBoardState, nextTurnPlayer);
            }

            var oppositePlayer = nextTurnPlayer == TPlayerColor.red ? TPlayerColor.black : TPlayerColor.red;
            var allOpponentPieces = GetPlayerTiles(oppositePlayer);
            if (allOpponentPieces.Count == 0)
            {
                currentState = new Winner(this, nextTurnPlayer);                                
            }
            else
            {
                var moveablePieces = GetMoveablePieces(nextTurnPlayer);

                if (moveablePieces.Count == 0)
                {
                    currentState = new Winner(this, oppositePlayer);                
                }
                else
                {
                    currentState = new ChoosePiece(this, nextTurnPlayer);                                    
                }
            }
        }

        string GetCurrentBoardPositions()
        {
            var ret = new StringBuilder();
            for (int yIndex = 0; yIndex < 8; yIndex++)
            {
                for (int xIndex = 0; xIndex < 8; xIndex++)
                {
                    var state = (int)tiles[xIndex, yIndex].CurrentState();
                    ret.Append(((int) state));
                }

                ret.AppendLine();
            }

            return ret.ToString();
        }
        void RestoreBoardPositions(string str)
        {
            string[] lines = str.Split(new char[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries);
            for (int yIndex = 0; yIndex < 8; yIndex++)
            {
                for (int xIndex = 0; xIndex < 8; xIndex++)
                {
                    var state = lines[yIndex][xIndex].ToString();
                    tiles[xIndex, yIndex].SetState(this, (Tile.TState)int.Parse(state));
                }
            }
            ResetComponents();
        }
    }
}